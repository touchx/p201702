<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>习近平主持金砖国家领导人非正式会晤</title>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<script type="text/javascript">
		wx.config({
		    debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
		    appId: '<?php echo $appId; ?>', // 必填，公众号的唯一标识
		    timestamp: <?php echo $timestamp; ?>, // 必填，生成签名的时间戳
		    nonceStr: '<?php echo $nonceStr; ?>', // 必填，生成签名的随机串
		    signature: '<?php echo $signature; ?>',// 必填，签名，见附录1
		    jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQQ'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
		});

		wx.ready(function(){
			wx.onMenuShareTimeline({
			    title: '百度新闻', // 分享标题
			    link: 'http://17216lg531.iask.in/P201702/wx/index.php/index/login/show.html', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
			    imgUrl: 'https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo/bd_logo1_31bdc765.png', // 分享图标
			    success: function () { 
			    	alert(888)
			        // 用户确认分享后执行的回调函数
			    },
			    cancel: function () { 
			    	alert(666)
			        // 用户取消分享后执行的回调函数
			    }
			});

			wx.onMenuShareAppMessage({
			    title: '这个是人珍上杜', // 分享标题
			    desc: '这个是人珍上杜这个是人珍上杜这个是人珍上杜这个是人珍上杜', // 分享描述
			    link: 'http://17216lg531.iask.in/P201702/wx/index.php/index/login/show.html', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
			    imgUrl: 'https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo/bd_logo1_31bdc765.png', // 分享图标
			    type: '', // 分享类型,music、video或link，不填默认为link
			    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
			    success: function () { 
			        // 用户确认分享后执行的回调函数
			    },
			    cancel: function () { 
			        // 用户取消分享后执行的回调函数
			    }
			});

			wx.onMenuShareQQ({
			    title: '这个是人珍上杜', // 分享标题
			    desc: '这个是人珍上杜这个是人珍上杜', // 分享描述
			    link: 'http://17216lg531.iask.in/P201702/wx/index.php/index/login/show.html', // 分享链接
			    imgUrl: 'https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo/bd_logo1_31bdc765.png', // 分享图标
			    success: function () { 
			       // 用户确认分享后执行的回调函数
			    },
			    cancel: function () { 
			       // 用户取消分享后执行的回调函数
			    }
			});
		});

		wx.error(function(res){
			// alert(666)
		});

		
	</script>


</head>
<body>
	新华社德国汉堡7月7日电（记者霍小光　蒋国鹏　刘畅）国家主席习近平7日在汉堡主持金砖国家领导人非正式会晤，发表引导性讲话和总结讲话。南非总统祖马、巴西总统特梅尔、俄罗斯总统普京、印度总理莫迪出席。5国领导人围绕世界政治经济形势和二十国集团重点议题深入交换意见，就金砖国家加强团结协作、合力构建开放型世界经济、完善全球经济治理、促进可持续发展达成重要共识。

　　习近平在讲话中指出，当前，世界经济出现一些积极迹象，金砖国家发展前景普遍向好，令人振奋；同时，也出现一些需要金砖国家密切关注、妥善应对的挑战。这次二十国集团领导人峰会正值世界政治经济形势面临深刻调整的重要时刻。金砖国家是二十国集团重要成员，要支持主席国德国办好汉堡峰会，发出二十国集团致力于加强伙伴关系、建设开放型世界经济、稳定和促进全球经济增长的积极信号。

　　习近平强调，金砖国家要发扬开放包容、合作共赢的伙伴精神，加强团结合作，维护共同利益，谋求联动发展。

　　第一，我们要毫不动摇构建开放型世界经济，维护多边贸易体制，推动经济全球化向开放、包容、普惠、平衡、共赢方向发展，让全体人民分享经济增长和经济全球化的成果。

　　第二，我们要毫不动摇奉行多边主义，共同推动各方以政治和和平方式解决地区冲突和争端，努力实现各国和平共处、合作共赢，倡导国际社会充分利用联合国等多边机制，共商国际规则，共建伙伴关系，共同应对各种全球性挑战。

　　第三，我们要毫不动摇加强全球经济治理，共同巩固二十国集团作为国际经济合作主要论坛的地位，推动主要经济体加强宏观经济政策协调，落实好二十国集团领导人杭州峰会及历届峰会共识，为新兴市场国家和发展中国家创造良好发展环境。

　　第四，我们要毫不动摇推动共同发展，推动二十国集团落实杭州峰会有关2030年可持续发展议程的行动计划以及支持非洲和最不发达国家工业化的倡议，带动国际社会向广大发展中国家特别是非洲国家提供支持，帮助他们提高发展能力，力争实现跨越式发展
<img src="https://imgsa.baidu.com/news/q%3D100/sign=5426ee808d01a18bf6eb164fae2e0761/0b46f21fbe096b6329d636fc06338744eaf8ac4b.jpg" alt="">
</body>
</html>