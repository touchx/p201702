<?php 

header("Content-type: text/html; charset=utf-8");

/**
 * [Ctime 转时间格式函数]
 * @Author   Xuebingsi
 * @DateTime 2017-05-08T08:32:40+0800
 * @param    [int]                   $time [传入的时间戳]
 */
function Ctime($time=null)
{
	if($time){

		$now = time();

		$diff = $now-$time;

		if($diff<60){
			return "刚刚";
		}elseif($diff<3600){
			return round($diff/60)."分钟前";
		}elseif($diff<86400){
			return round($diff/3600)."小时之前";
		}elseif($diff<(86400*7)){
			return round($diff/86400)."天之前";
		}else{
			return date("Y-m-d H:i:s",$time);
		}
	}else{
		return "请传入想要转换的时间戳";
	}
}

/**
 * [F 转存储单位格式]
 * @Author   Xuebingsi
 * @DateTime 2017-05-08T08:39:16+0800
 * @param    [int]                   $size [字节]
 */
function F($size)
{
	if($size<1024){
		return $size."B";
	}elseif($size<pow(1024, 2)){
		return round($size/1024,2)."Kb";
	}elseif($size<pow(1024, 3)){
		return round($size/pow(1024, 2),2)."Mb";
	}else{
		return round($size/pow(1024, 3),2)."Gb";
	}
}
/**
 * [M 实例化数据库链接对象]
 * @Author   Xuebingsi
 * @DateTime 2017-05-24T08:20:52+0800
 * @param    string                   $table [表名]
 */
function M($table='')
{
	return new Model($table);
}

/**
 * [D 实例化模型]
 * @Author   Xuebingsi
 * @DateTime 2017-05-25T12:18:52+0800
 * @param    string                   $table [模板名]
 */
function D($table='')
{
	$Model = ucfirst($table)."Model";
	return new $Model($table);
}

/**
 * [C 获取配置的方法]
 * @Author   Xuebingsi
 * @DateTime 2017-05-24T08:34:34+0800
 * @param    string                   $index [配置项]
 */
function C($index='')
{
	$configs = include CONFIG_PATH.'config.php';
	if(empty($index)){
		return  $configs;
	}else{
		return  $configs[$index];
	}
}











 ?>