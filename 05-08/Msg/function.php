<?php 

header("Content-type: text/html; charset=utf-8");

/**
 * [Ctime 转时间格式函数]
 * @Author   Xuebingsi
 * @DateTime 2017-05-08T08:32:40+0800
 * @param    [int]                   $time [传入的时间戳]
 */
function Ctime($time=null)
{
	if($time){

		$now = time();

		$diff = $now-$time;

		if($diff<60){
			return "刚刚";
		}elseif($diff<3600){
			return round($diff/60)."分钟前";
		}elseif($diff<86400){
			return round($diff/3600)."小时之前";
		}elseif($diff<(86400*7)){
			return round($diff/86400)."天之前";
		}else{
			return date("Y-m-d H:i:s",$time);
		}
	}else{
		return "请传入想要转换的时间戳";
	}
}

/**
 * [F 转存储单位格式]
 * @Author   Xuebingsi
 * @DateTime 2017-05-08T08:39:16+0800
 * @param    [int]                   $size [字节]
 */
function F($size)
{
	if($size<1024){
		return $size."B";
	}elseif($size<pow(1024, 2)){
		return round($size/1024,2)."Kb";
	}elseif($size<pow(1024, 3)){
		return round($size/pow(1024, 2),2)."Mb";
	}else{
		return round($size/pow(1024, 3),2)."Gb";
	}
}











 ?>