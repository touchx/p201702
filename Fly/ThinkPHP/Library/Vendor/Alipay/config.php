<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2017040606564689",

		//商户私钥
		'merchant_private_key' => "MIIEpQIBAAKCAQEAvx0QeymGkC9tR/d3cmEn1wQcXp/gJPRT8GxUZRsE8EM7c8fI37RbwFBu7kkKHiZzgDNg34Ktu7CMJeEGV6OaDLxsiI5y0FQa9ufEn4P74CtzOQSRV9rPz13PvAlsW/AZYjU2XSc3wCCPZEAsKMg0ewEj7CuBR86N3+Pd8u+9Z4/rIVTYJi6jcURO1G0Fd6FMd4VFZoGNCDRhmQzxwrpYuNjqPyXWlyR+ZaQQiph2/JgdYd42H1DSsafpg85ev/9tCU1L20DyFgJKpl9ct4Xe9kb87+HFFS25pfEiWJA9KdnnmREsFmTbmG59PshmYh0cnVS6WRMGscEOklJxEo2r6wIDAQABAoIBAQCnD7gdgcJQfjTwSSIQSV+zNLO7/9PHWhGaRIhNzZ22jX8GCenNq/LcnTMrd4uxqXkj40XWfHx0arNIVXne2HPMmFXj1WoGNM9RcG7P5mciobjaaeTHwM4R/naecDpZ7/uwfsWkDqlMeOn//5zalZBSyj9lru4XIDejNKULPYj7Yi4MFnMeA5eAQ4uKzgkJqIkZ4KtS0ku3s5Y0cLRgaIdFa3cBBIwgFbxw3a9XsaFxgCoAZLlm8M+/YuOmAzfkoa6CCUyEscJaf9Z8MAJV5LFgZ4/b0d+OrwD68K/2uv5xqriW8poMUueDDBuorA4OyrevdZqz4HbLx+ElpssNzBRxAoGBAPLE/YhX4Doy7nPJyc0qgXopMCcTCrnSiuGEMbV6Fj666HgHxv9tJq3486U9/JuA5BsC7Ceq49h/36w3tSbEHaVXJIr1oj++xdGZa2B95HYsb54tFwwLco4HZfkOibSeFC8rGfBV0pVwBFiBau+XuvPKKsybSfZQ4i7AFwdosIyDAoGBAMmHZL5MVbAn6XCkrz238om/eSk7S5yN9bCcK2ucIdpgqiGcK4gd3CL7s2PKVQsU9F9QjtZTmT6QDqjVeNC0+ySfAyFPlhy/7NDwN8siQn7jul4yN4flBztAKPAjg/u4cH/Ib4OTbz7jQZjVjN9PppETaoRFn/Dw3yyMduwKWBZ5AoGBAK4O5ug2eSHBGo4SA4oCC8sWdNvPH0WT2tdN/IXs/r4DLGHC0b6Y976MW8QB6XdktidXtRHLyIiVRj0nw84Dlm3RSPYIn0es9BAPO5l3KbJAsdZq5bdRDdUfINGsxralEDuxX2udXNqf8b2zLIST+QqJzJ3eSO09mK2EM/6v5MUZAoGBAJ/tOh1qJnXstmwVxJuN5oM518TIsXBDl73XsP1sodDS9TBG5qTWTWPVqr5+So7IykT5gODE5s+nMnr5LS8y8DpXlfxRVCbsh9onR5+ESJC516P45eiJ7/stJ0+qX4rbCyvcu+IGljmpztyiW3C7nMt0RQtC1XI3NGP99EHM06rBAoGAXsoRV8w15D/65l22BIHbko2KuAkXWq/NZNKzLele9N+Tie2SfkjtcnoRqEC8EXbvu4m9vJ+DzQirPXik0PU1X+fPRXyoNQVtVdUwhU6tqya/ntYzaR7CGVXETFHyJUpjOm37EPTyF7i5R8Buwt+uK9HyjsWNdyNmRaXLjAp0vwg=",
		
		//异步通知地址
		'notify_url' => "http://17216lg531.iask.in/P201702/Fly/index.php/index/pay/paynotice",
		
		//同步跳转
		'return_url' => "http://17216lg531.iask.in/P201702/Fly/index.php/index/pay/payreturn",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm934Ug/ZD1/pRyAxn0ba2L3jxkbeU87tRybaMsCnlnuCo43PbWM/6NSOjDzqstUkBk8jrQTKMxow0F1pmboYEg4lB2YkzbnO+365ou48CrdSa9DaDcyft9RGgJ6VZmp2WHMeO2av6a7ET5IlOqcVs/UYCz4QyncQwCeTV52Antpm4bMEExGRpY5w+5+vvA16gCeY14f2/eTrueVH+QRge1F5sQe1mJxaNj29igc84f0iu19+YCOBZAeUDSfsxy2Ggqtam0TtqycRHYasH4L/PdR0YmJAXlUHlX7EbeJriDL6CfYyLWmxYjTPnRKFpA49bbSAkup0EaKEa9oIB6ZD6wIDAQAB",
);