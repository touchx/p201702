<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            X-admin v1.0
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="/P201702/Fly/Public/Admin/css/x-admin.css" media="all">
        <script src="/P201702/Fly/Public/Admin/lib/layui/layui.js" charset="utf-8"></script>
    </head>
    <body>
        <div class="x-body">
            <form class="layui-form" method="post" action="<?php echo U('admin/goods/addattr');?>">

                <?php foreach ($attrs as $attr) { ?>
                    
                
                 <div class="layui-form-item">
                    <label class="layui-form-label"><?php echo $attr['attr_name'] ?></label>
                    <div class="layui-input-block">
                        <?php foreach ($attr['attr_value'] as $v) { ?>
                            
                        
                      <input type="checkbox" name="attr[<?php echo $attr['attr_id'] ?>][]" value="<?php echo $v ?>" title="<?php echo $v ?>" <?php if(in_array($v,$attrArr)){ echo "checked";} ?> >
                      <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <div class="layui-form-item">
                    <input type="hidden" value="<?php echo $_GET['id'] ?>" name="good_id">
                    <label for="L_repass" class="layui-form-label">
                    </label>
                    <input type="submit"  class="layui-btn" value="增加">
                </div>
            </form>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8">
        </script>
        <script src="./js/x-layui.js" charset="utf-8">
        </script>
        <script>
            layui.use(['form','layer'], function(){
                $ = layui.jquery;
              var form = layui.form()
              ,layer = layui.layer;
              
            });
        </script>
    </body>
</html>