<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            X-admin v1.0
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="/P201702/Fly/Public/Admin/css/x-admin.css" media="all">
        <script src="/P201702/Fly/Public/Admin/lib/layui/layui.js" charset="utf-8"></script>
    </head>
    <body>
        <script src="https://cdn.bootcss.com/echarts/3.6.1/echarts.min.js"></script>
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<div class="x-body">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div id="xbs" style="width: 600px;height:400px;"></div>
    <div id="xbs1" style="width: 600px;height:400px;"></div>
</div>
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('xbs'));

        // 指定图表的配置项和数据
        var option = {
            title: {
                text: '最近一周注册用户统计表'
            },
            tooltip: {},
            legend: {
                data:['注册量']
            },
            xAxis: {
                data: [<?php echo $cday ?>]
            },
            yAxis: {},
            series: [{
                name: '注册量',
                type: 'bar',
                data: [<?php echo $num ?>]
            }]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>

    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('xbs1'));

        $.ajax({
            url: '<?php echo U('admin/sysconut/regajax');?>',
            type: 'get',
            dataType: 'json',
            async: false
        })
        .done(function(data) {

            num =[];
            days=[];
            for( i in data){
                num.push(data[i].cn);
                days.push(data[i].cday);
            }

            console.log(num);
            console.log(days);
        })
        

        option = {
            title: {
                text: '最近一周注册用户统计表',
                subtext: ''
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data:['注册量']
            },
            toolbox: {
                show: true,
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    dataView: {readOnly: false},
                    magicType: {type: ['line', 'bar']},
                    restore: {},
                    saveAsImage: {}
                }
            },
            xAxis:  {
                type: 'category',
                boundaryGap: false,
                data: days
            },
            yAxis: {
                type: 'value',
                axisLabel: {
                    formatter: '{value} 人'
                }
            },
            series: [
                {
                    name:'最多注册量',
                    type:'line',
                    data:num,
                    markPoint: {
                        data: [
                            {type: 'max', name: '最大值'},
                            {type: 'min', name: '最小值'}
                        ]
                    },
                    markLine: {
                        data: [
                            {type: 'average', name: '平均值'}
                        ]
                    }
                }
            ]
        };


        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
    </body>
</html>