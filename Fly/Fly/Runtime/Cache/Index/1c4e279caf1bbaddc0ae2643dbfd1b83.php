<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>
            <?php echo $title ?>-<?php echo C('SITE_NAME') ?>
        </title>
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/font.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/layui.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/global.css">
        <script src="/P201702/Fly/Public/Index/layui.js"></script>
    </head>
    
    <body>
        <div class="header">
            <div class="main">
                <a class="logo" href="/P201702/Fly" title="Fly">
                    Fly社区
                </a>
                <div class="nav">
                    <a href="/jie/">
                        <i class="iconfont icon-wenda">
                        </i>
                        讨论
                    </a>
                    <a href="/case/2017/">
                        <i class="iconfont icon-iconmingxinganli" style="top: 2px;">
                        </i>
                        案例
                    </a>
                    <a href="http://www.layui.com/">
                        <i class="iconfont icon-ui">
                        </i>
                        框架
                    </a>
                </div>
                <?php if(isset($_SESSION['uid'])){ ?>
                    <div class="nav-user">
                    <a class="avatar" href="/user/">
                        <img id="face" src="<?php echo $_SESSION['face'] ?>">
                        <cite>
                            <?php echo $_SESSION['nickname'] ?>
                        </cite>
                    </a>
                    <div class="nav">
                        <a href="/user/set/">
                            <i class="iconfont icon-shezhi">
                            </i>
                            设置
                        </a>
                        <a href="<?php echo U('index/login/out');?>">
                            <i class="iconfont icon-tuichu" style="top: 0; font-size: 22px;">
                            </i>
                            退了
                        </a>
                    </div>
                </div>
                <?php }else{ ?>

               
                <div class="nav-user">
                    <a class="unlogin" href="<?php echo U('index/login/index');?>">
                        <i class="iconfont icon-touxiang">
                        </i>
                    </a>
                    <span>
                        <a href="<?php echo U('index/login/index');?>">
                            登入
                        </a>
                        <a href="<?php echo U('index/reg/index');?>">
                            注册
                        </a>
                    </span>
                    <p class="out-login">
                        <a href="<?php echo U('index/login/qqlogin');?>" 
                        class="iconfont icon-qq" title="QQ登入">
                        </a>
                        <a href="http://fly.layui.com:8098/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
                        class="iconfont icon-weibo" title="微博登入">
                        </a>
                    </p>
                </div>

                 <?php } ?>
                
            </div>
        </div>
        <div class="main layui-clear">
    <div class="fly-panel" pad20 style="padding-top: 5px;">
        <div class=" layui-form-pane">
            <div class="layui-tab layui-tab-brief" lay-filter="user">
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        发表新帖
                    </li>
                </ul>
                <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                    <div class="layui-tab-item layui-show">
                        <form action="" method="post">
                            <div class="layui-form-item">
                                <label for="L_title" class="layui-form-label">
                                    标题
                                </label>
                                <div class="layui-input-block">
                                    <input type="text" id="L_title" name="title" required lay-verify="required"
                                    autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-text">
                                <div class="layui-input-block">
                                    <textarea id="L_content" name="content" 
                                    placeholder="请输入内容" class="layui-textarea fly-editor" style="height: 260px;"></textarea>
                                </div>
                                <label for="L_content" class="layui-form-label" style="top: -2px;">
                                    描述
                                </label>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">
                                        所在类别
                                    </label>
                                    <div class="layui-input-block">
                                        <select lay-verify="required" name="cid">
                                            <option></option>

                                            <?php foreach ($cate as $row) { ?>
                                                
                                            
                                            <optgroup label="<?php echo $row['name'] ?>">

                                                <?php foreach ($row['son'] as $cateson) { ?>
                                                    <option value="<?php echo $cateson['id'] ?>"><?php echo $cateson['name'] ?></option>
                                                <?php } ?>
                                            </optgroup>

                                            <?php } ?>

                                           
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">
                                        悬赏飞吻
                                    </label>
                                    <div class="layui-input-block">
                                        <select name="kiss">
                                            <option value="5">5</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">
                                    人类验证
                                </label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_vercode" name="vercode" required lay-verify="required"
                                    placeholder="请回答后面的问题" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid">
                                    <span style="color: #c00;">
                                        <?php echo $question ?>
                                    </span>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <button class="layui-btn" lay-filter="add" lay-submit>
                                    立即发布
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //一般直接写在一个js文件中
    layui.use(['layer','form','jquery','layedit'], function(){
      
        var layer = layui.layer;
        var form = layui.form();
        var $ = layui.jquery;
        var layedit = layui.layedit;

        layedit.set({
            uploadImage: {
            url: '<?php echo U('index/jie/upload');?>' //接口url
            ,type: '' //默认post
            }
        });

        index = layedit.build('L_content'); //建立编辑器


        //监听提交
        form.on('submit(add)', function(data){

            content = layedit.getContent(index);

            if(content.length<20){
                layer.msg("内容不能少于20长度");
                return false;
            }

            data.field.content=content;
            // console.log(data.field);
            //发异步
            $.ajax({
                url: '<?php echo U('index/jie/addjie');?>',//异步地址
                type: 'POST',
                dataType: 'json',
                data: data.field,//发送的数据
            })
            .done(function(respones) {
                //失败
                if(respones.error==1){
                    // 把失败信息弹出
                    layer.msg(respones.info, function(){});
                }else{
                    //把成功信息弹出，并中转到登录页面
                    layer.alert(respones.info.info, {icon: 6},function  () {
                        location.replace(respones.info.url)
                    })
                }
            })
            .fail(function() {
                console.log("error");
            })
            
            return false;
        });

    });
</script>
        <div class="footer">
            <p>
                <a href="http://fly.layui.com/">
                    Fly社区
                </a>
                2017 &copy;
                <a href="http://www.layui.com/">
                    layui.com
                </a>
            </p>
            <p>
                <a href="http://fly.layui.com/jie/3147.html" target="_blank">
                    产品授权
                </a>
                <a href="http://fly.layui.com/jie/8157.html" target="_blank">
                    获取Fly社区模版
                </a>
                <a href="http://fly.layui.com/jie/2461.html" target="_blank">
                    微信公众号
                </a>
            </p>
        </div>
    </body>
</html>