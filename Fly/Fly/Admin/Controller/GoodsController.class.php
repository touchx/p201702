<?php
namespace Admin\Controller;
use Think\Controller;
class GoodsController extends CommonController {
    public function index(){

            $goods = M('shop_goods')->select();
            $this->assign('goods',$goods);
            $this->display();
    }
    public function add()
    {
        
        if($_POST){
           $data= I("post.");

           $res = M('shop_goods')->add($data);

           if($res){
                $this->success("增加成功",U('admin/goods/index'));
           }


        }else{
            $cate = M('shop_category')->select();
            $this->assign('cate',$cate);
            $this->display(); 
        }
        
    }

    public function addattr()
    {
        if($_POST){
             $data =I('post.attr');
             $good_id = I('post.good_id');

             foreach ($data as $attr_id => $row) {
                 foreach ($row as  $v){
                    $insertData['good_attr_value']=$v;
                    $insertData['shop_type_attr_id']=$attr_id;
                    $insertData['good_id']= $good_id;
                    M('shop_goods_attr')->add($insertData);
                 }
             }

             $this->success("增加成功");

        }else{


            $goodsId = I('get.id');
            $sql ='select t3.* from shop_goods t1 INNER JOIN shop_category t2 on t1.cate_id=t2.cid INNER JOIN shop_type_attr t3 on t2.type_id=t3.type_id where t1.good_id="'.$goodsId.'" and t3.isbeselected="1"';
            $attrs = M()->query($sql);



            foreach ($attrs as $k => $v) {
                $v['attr_value'] = explode(',', $v['attr_value']);

                $attrs[$k]=$v;
            }

            $attr = M("shop_goods_attr")->where("good_id='{$goodsId}'")->select();

            $attrArr = [];

            foreach ($attr as $row) {
                $attrArr[]=$row['good_attr_value'];
            }

            // var_dump($attrs);
            $this->assign('attrs',$attrs);
            $this->assign('attrArr',$attrArr);
            $this->display();
        }
    }

    public function addsku()
    {
        if($_POST){
            $data = I('post.');

            $data['attr_combine'] = implode(',', $data['attr']);

            M('shop_goods_list')->add($data);

            $this->success("增加sku成功");
        }else{
            $goodsId = I('get.id');

            $sql ="select t1.*,t2.attr_name from shop_goods_attr t1 INNER JOIN shop_type_attr t2 on t1.shop_type_attr_id=t2.attr_id where good_id='{$goodsId}'";

            $attrs = M()->query($sql);

            $tempArr = [];

            foreach ($attrs as $attr) {
                $tempArr[$attr['attr_name']][]=$attr;
            }



            $list = M('shop_goods_list')->where("good_id='{$goodsId}'")->select();

            foreach ($list as $k => $row) {
                $sql = "select GROUP_CONCAT(good_attr_value) good_attr_value from shop_goods_attr where good_attr_id in(".$row['attr_combine'].")";
                $row['good_attr_value']=current(M()->query($sql))['good_attr_value'];

                $list[$k]=$row;
            }
            $this->assign('list',$list);
            $this->assign('attrs',$tempArr);
            $this->display();
        }
    }
}