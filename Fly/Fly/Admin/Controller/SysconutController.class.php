<?php
namespace Admin\Controller;
use Think\Controller;
class SysconutController extends CommonController {
	public function reg()
	{

		$days= [];

		for ($i=7; $i >0 ; $i--) { 
			$days[]=date("m-d",strtotime("-".$i." days"));
		}

		$time1  = strtotime(date("Y-m-d",strtotime("-7 days")));

		$time2  = strtotime(date("Y-m-d",time()));


		$sql = "select count(id) cn,FROM_UNIXTIME(create_time, '%m-%d') cday from user where create_time >'$time1' and create_time < '$time2' group by cday";

		$data = M()->query($sql);

		$cday = "";
		$num  = "";

		// var_dump($data);
		// var_dump($days);


		foreach ($days as $day) {
			$cn = 0;
			foreach ($data as $row) {
				if($day==$row['cday']){
					$cn = $row['cn'];
				}
			}

			$cday .="'".$day."',";
			$num .=$cn.",";
		}


		$cday = rtrim($cday,',');
		$num = rtrim($num,',');

		// $cday = "'06-02','06-03','06-04','06-05','06-06','06-07','06-08'";
		// $num ="0,3,1,1,7,8,1";


		// var_dump($cday,$num);

		// exit;



		$this->assign('cday',$cday);
		$this->assign('num',$num);
		$this->display();
	}

	public function regajax()
	{
		$time1  = strtotime(date("Y-m-d",strtotime("-7 days")));

		$time2  = strtotime(date("Y-m-d",time()));


		$sql = "select count(id) cn,FROM_UNIXTIME(create_time, '%m-%d') cday from user where create_time >'$time1' and create_time < '$time2' group by cday";

		$data = M()->query($sql);


		echo json_encode($data);
	}
}