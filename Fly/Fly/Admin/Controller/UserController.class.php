<?php
namespace Admin\Controller;
use Think\Controller;
class UserController extends CommonController {
    public function userlist(){

        $where = "is_del='0'";

        if(I('post.start')){
            $where .= " and create_time > '".strtotime(I('post.start'))."'";
        }

        if(I('post.end')){
            $where .= " and create_time < '".strtotime(I('post.end'))."'";
        }

        if(I('post.username')){
            $where .= " and nickname ='".I('post.username')."'";
        }

        $userlist = M('user')->where($where)->select();
        $this->assign('userlist',$userlist);
        $this->display();
    }

    public function add()
    {
        echo "这个是add方法";
    }

    public function show()
    {
        
        $user= M('user')->find(I('get.id'));
        $this->assign('user',$user);
        $this->display();
    }

    public function stop()
    {
        $data['status']=1;
        $data['id'] = I('post.uid');

        if(M('user')->save($data)){
            ReAjax(0,'停用成功');
        }else{
            ReAjax(1,'停用失败');
        }
    }
    public function start($value='')
    {
        $data['status']=0;
        $data['id'] = I('post.uid');

        if(M('user')->save($data)){
            ReAjax(0,'启用成功');
        }else{
            ReAjax(1,'启用失败');
        }
    }
    public function delall()
    {
        $uids = I('post.uids');

        $uids= rtrim($uids,',');

        $sql  = "delete from user where id in (".$uids.")";

        ReAjax(0,'删除成功');
    }


    public function delrule()
    {
        $id = '1';

        // M('auth_rule')->delete($id);

        $group = M('auth_group')->select();

        var_dump($group);

        foreach ( $group as $key => $row) {

            $rules = explode(',', $row['rules']);

            $key = array_search($id, $rules);

            if($rules[$key]==$id){
                unset($rules[$key]);
            }

            $row['rules']= implode(',', $rules);

            M('auth_group')->save($row);
        }
    }

    public function addrole()
    {
        $rules = M('auth_rule')->select();

        $arr = [];
        foreach ($rules as $row) {
             $arr[$row['cname']][]=$row;
        }

        var_dump($arr);
        $this->assign('rules',$arr);
        $this->display();
    }

    
}