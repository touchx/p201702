<?php
namespace Admin\Controller;
use Think\Controller;
class AuthController extends CommonController {
	public function category()
	{
		$cdata = M('auth_category')->select();
		$this->assign('cdata',$cdata);
		$this->display();
	}
	public function catedel()
	{
		$id = I('post.id');
		if(M('auth_category')->delete($id)){
			$this->log(json_encode(array('删除分类',"分类".$id)));
			ReAjax(0,'删除成功');
		}else{
			ReAjax(1,'删除失败');
		}
	}
	public function cateadd()
	{
		$data =  I('post.');
		if(M('auth_category')->add($data)){
			ReAjax(0,'增加成功');
		}else{
			ReAjax(1,'增加失败');
		}
	}
	public function role()
	{
		$gdata = M('auth_group')->select();

		foreach ($gdata as $k => $row) {
			// $gdata[$k]['rtitle'] = current(M()->query("select GROUP_CONCAT(title) rtitle  from auth_rule where id in (".$row['rules'].")"))['rtitle'];
			// 
			if($row['rules']){
				$gdata[$k]['rtitle'] = M('auth_rule')->field("GROUP_CONCAT(title) rtitle")->where("id in (".$row['rules'].") ")->find()['rtitle'];
			}
			
		}
		$this->assign('gdata',$gdata);
		$this->display();
	}
	public function roleaddshow()
	{
		$rdata = M('auth_rule t1')->join('auth_category t2 on t1.cid=t2.id')->field('t1.*,t2.name cname')->select();
		$tempArr = array();

		foreach ($rdata as $row) {
			$tempArr[$row['cname']][]=$row;
		}
		$this->assign('rdata',$tempArr);
		$this->display();
	}

	public function roleadd()
	{
		$data = I('post.');
		$data['rules'] =implode(',', $data['id']);
		if(M('auth_group')->add($data)){
			ReAjax(0,'增加成功');
		}else{
			ReAjax(1,'增加失败');
		}
	}
	public function roledel()
	{
		$id = I('post.id');
		$role = M('auth_group')->find($id);
		if(M('auth_group')->delete($id)){
			$this->log(json_encode(array('删除分类',$role)));
			ReAjax(0,'删除成功');
		}else{
			ReAjax(1,'删除失败');
		}
	}

	public function roleedit()
	{
		$id = I('get.id');

		$role = M('auth_group')->find($id);

		$rdata = M('auth_rule t1')->join('auth_category t2 on t1.cid=t2.id')->field('t1.*,t2.name cname')->select();
		$tempArr = array();

		foreach ($rdata as $row) {
			$tempArr[$row['cname']][]=$row;
		}

		$rules = explode(',', $role['rules']);

		$this->assign('rules',$rules);
		$this->assign('role',$role);
		$this->assign('rdata',$tempArr);
		$this->display();
	}

	public function rolesave()
	{
		$data = I('post.');
		$data['rules'] =implode(',', $data['rules']);
		if(M('auth_group')->save($data)){
			ReAjax(0,'增加成功');
		}else{
			ReAjax(1,'增加失败');
		}
	}
	public function rule()
	{
		$rdata = M('auth_rule t1')->join('auth_category t2 on t1.cid=t2.id')->field('t1.*,t2.name cname')->select();
		$cdata = M('auth_category')->select();
		$this->assign('cdata',$cdata);
		$this->assign('rdata',$rdata);
		$this->display();
	}
	public function ruledel()
	{
		$id = I('post.id');
		if(M('auth_rule')->delete($id)){

			$gdata = M('auth_group')->select();

			foreach ($gdata as $key => $row) {

				$rules = explode(',', $row['rules']);

				if(in_array($id, $rules)){

					$key = array_search($id, $rules);

					if($rules[$key]==$id){

						unset($rules[$key]);

						$row['rules'] = implode(',', $rules);

						M('auth_group')->save($row);
					}

				}
			}

			ReAjax(0,'删除成功');
		}else{
			ReAjax(1,'删除失败');
		}
	}
	public function ruleadd()
	{
		$data =  I('post.');
		if(M('auth_rule')->add($data)){
			ReAjax(0,'增加成功');
		}else{
			ReAjax(1,'增加失败');
		}
	}
	public function admin()
	{
		$this->display();
	}
	public function adminaddshow()
	{
		$this->display();
	}
}