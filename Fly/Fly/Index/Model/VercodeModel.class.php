<?php 
namespace Index\Model;
use Think\Model;
/**
 * 人类验证模型
 */
class VercodeModel extends Model {
	//随机获取一条问题
	public function getOne()
	{

		$res =M()->query("select * from vercode order by rand() limit 1");
		//把答案存到session
		$_SESSION['vercode'] = current($res)['answer'];
		//把问题返回
		return current($res)['question'];
	}
	//检查人类验证
	public function checkCode($code)
	{	
		// 判断session中的验证码与用户异步传过来的验证码是否一致
		if($_SESSION['vercode']!=$code){
			ReAjax(1,"人类验证不通过");
		}
	}
}

 ?>