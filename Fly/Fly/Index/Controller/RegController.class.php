<?php
namespace Index\Controller;
use Think\Controller;
/**
 * 注册控制器
 */
class RegController extends CommonController {
    //注册首页
    public function index(){
        if(isset($_SESSION['uid'])){
            $this->error('你已经登录',U('index/user/index'));
        }
        //随机获取一条问题
    	$question = D('vercode')->getOne();
        //分配变量
    	$this->assign('question',$question);
        //载入视图
        $this->assign('title',"注册");
        $this->display();
    }
    //接收异步数据进行验证
    public function add()
    {   
        //判断人类验证
    	D('vercode')->checkCode(I('post.vercode'));

        //判断两次密码
    	if(I('post.pass')!=I('post.repass')){
    		ReAjax(1,"两次密码不一致");
    	}

    	D('user')->checkEamil(I('post.email'));

    	D('user')->checkNickname(I('post.nickname'));

        //接收数据
    	$data = I('post.');
        //增加创建时间
    	$data['create_time']=time();
        //加密
    	$data['password'] = md5($data['pass']);

        $arr = ['/Public/Index/images/face.gif','/Public/Index/images/face.jpg'];

        $data['face'] = $arr[mt_rand(0,1)];
        // 增加到数据库
    	if(M('user')->add($data)){
    		ReAjax(0,"注册成功");
    	}else{
    		ReAjax(1,"注册失败，请重新注册");
    	}

    }
}