<?php
namespace Index\Controller;
use Think\Controller;
class UserController extends CommonController {
    public function index(){
        $this->display();
    }
    public function set()
    {
    	//获取用户数据
    	$data = M('user')->find($_SESSION['uid']);
    	//分配数据
		$this->assign('user',$data);
		$this->display();
    }
    public function msg()
    {
    	$this->display();
    }

    public function useredit()
    {	//接收数据
    	$data = I('post.');

    	$email = $data['email'];

    	$id = $_SESSION['uid'];

    	$data['id']=$id;
    	//判断邮箱其它人有没有用
    	$res = M('user')->where("email='$email' and id <> '$id'")->find();

    	// $sql = "select * from user where email='$email' and id <> '$id'";

    	if($res){
    		ReAjax(1,'邮箱已存在');
    	}
    	
    	if(M('user')->save($data)){
    		ReAjax(0,'修改成功');
    	}else{
    		ReAjax(1,'修改失败');
    	}
    }
    public function upload()
    {
    	$upload = new \Think\Upload();// 实例化上传类
	    $upload->maxSize   =     3145728 ;// 设置附件上传大小
	    $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
	    $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
	    $upload->savePath  =     ''; // 设置附件上传（子）目录
	    // 上传文件 
	    $info   =   $upload->upload();
	    // var_dump($info);

	    if(!$info) {// 上传错误提示错误信息
	        $this->error($upload->getError());
	    }else{// 上传成功
	    	//二维数组转一维
	    	$info = current($info);
	    	//组头像路径
	        $data['face'] = "/Uploads/".$info['savepath'].$info['savename'];
	        //获取用户id
	        $data['id']=$_SESSION['uid'];
	        //把头像地址更改到session
	        $_SESSION['face']=  $data['face'];
	        //把头像地址更改到数据库
	        M('user')->save($data);
	        //返回头像
	        echo $data['face'];
	    }
    }
    public function send()
    {

        $content = <<<xbs
        <div></div><div><includetail><table border="0" cellpadding="0" cellspacing="0" style="width: 800px; border: 1px solid #ddd; border-radius: 3px; color: #555; font-family: 'Helvetica Neue Regular',Helvetica,Arial,Tahoma,'Microsoft YaHei','San Francisco','微软雅黑','Hiragino Sans GB',STHeitiSC-Light; font-size: 12px; height: auto; margin: auto; overflow: hidden; text-align: left; word-break: break-all; word-wrap: break-word;"><tbody style="margin: 0; padding: 0;"><tr style="background-color: #393D49; height: 60px; margin: 0; padding: 0;"><td style="margin: 0; padding: 0;"><div style="color: #5EB576; margin: 0; margin-left: 30px; padding: 0;"><a style="font-size: 14px; margin: 0; padding: 0; color: #5EB576; text-decoration: none;" href="http://fly.layui.com/" target="_blank">老司机 - 老司机论坛</a></div> </td> </tr> <tr style="margin: 0; padding: 0;"> <td style="margin: 0; padding: 30px;"> <p style="line-height: 20px; margin: 0; margin-bottom: 10px; padding: 0; font-size: 14px;"> <em>Hi，可爱的码农：</em> </p> <p style="line-height: 20px; margin: 0; margin-bottom: 10px; padding: 0; font-size: 14px;">  这个是一一个老司机论坛的注册邮件 </p> <div style=""> <a href="http://layim.layui.com/" style="background-color: #009E94; color: #fff; display: inline-block; height: 32px; line-height: 32px; margin: 0 15px 0 0; padding: 0 15px; text-decoration: none;" target="_blank">获得LayIM</a> </div> </td> </tr> <tr style="background-color: #fafafa; color: #999; height: 35px; margin: 0; padding: 0; text-align: center;"> <td style="margin: 0; padding: 0;">Fly前端社区 © layui.com</td> </tr> </tbody> </table></includetail></div>
xbs;


        sendmail('6955044@qq.com','活动通知',$content);
    }
}