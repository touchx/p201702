<?php
namespace Index\Controller;
use Think\Controller;
class JieController extends CommonController {
    public function index(){

    	$id = I('get.id');

        $view = $this->addview($id,1);

        var_dump($view);

        $uid = isset($_SESSION['uid'])?$_SESSION['uid']:'';

    	$sql = "select t1.*,t2.face,t2.nickname,(select uid from collect where uid='$uid' and qid='$id') is_coll from question t1 left join user t2 on t1.uid=t2.id where t1.id='$id'";

        // $data = M('question t1')->join('user t2 on t1.uid=t2.id')->where("t1.id='$id'")->field('t1.*,t2.face,t2.nickname')->find();

        // var_dump( $data);

    	$data = M()->query($sql);

        $hotview=D('question')->hotview();



        $sql = "select t1.*,t2.face,t2.nickname,(select uid from zan where uid='$uid' and aid=t1.id) is_zan from answer t1 inner join user t2 on t1.uid=t2.id where t1.qid='$id'";

        $answer = M()->query($sql);

        // var_dump($answer);



        $this->assign('answer',$answer);
        $this->assign('hotview',$hotview);
    	$this->assign('ques',current($data));
        $this->assign('title',current($data)['title']);
        $this->display();
    }
    public function add()
    {
    	$cate = M('category')->select();

    	// var_dump($cate);

    	$tempArr = [];
    	// 是获取顶级分类
    	foreach ($cate as $row) {
    		if($row['fid']==0){
    			$tempArr[$row['id']]=$row;
    		}
    	}

    	foreach ($cate as $son) {
    		foreach ($tempArr as $key =>$fater) {
    			if($son['fid']==$key){
    				$tempArr[$key]['son'][]=$son;
    			}
    		}
    	}

    	//随机获取一条问题
    	$question = D('vercode')->getOne();
        //分配变量
    	$this->assign('question',$question);
    	$this->assign('cate',$tempArr);

    	$this->display();
    }

    public function addjie()
    {
    	// var_dump(I('post.'));
    	//判断人类验证
    	D('vercode')->checkCode(I('post.vercode'));

        $kiss = I('post.kiss');

        $uid = $_SESSION['uid'];

        M('user')->where("id='$uid'")->setDec('kiss',$kiss);

        // update user set kiss=kiss-10 where id =12;
    	$data = I('post.');
    	$data['uid']= $uid;
    	$data['create_time']=time();

    	if($id = M('question')->add($data)){
    		$url = U('Index/jie/index',array('id'=>$id));
    		ReAjax(0,array('info'=>'发布成功','url'=>$url));
    	}else{
			ReAjax(1,"发布失败");
    	}
    }

    public function upload()
    {
    	$upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     3145728 ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =     './Uploads/jie/'; // 设置附件上传根目录
        $upload->savePath  =     ''; // 设置附件上传（子）目录
        // 上传文件 
        $info   =   $upload->upload();
        // var_dump($info);

        if(!$info) {// 上传错误提示错误信息
            $this->error($upload->getError());
        }else{// 上传成功
            //二维数组转一维
            $info = current($info);
            //组头像路径
            $img = __ROOT__."/Uploads/jie/".$info['savepath'].$info['savename'];

            $arr =array(
                'code'=>0,
                'msg'=>'成功',
                'data'=>array(
                    'src'=>$img
                )
            );

            echo json_encode($arr);
        }
    }

    public function collect()
    {
        $qid = I('post.qid');

        $uid = $_SESSION['uid'];

        $res = M('collect')->where("qid='$qid' and uid='$uid'")->find();

        if($res){
            M('collect')->where("qid='$qid' and uid='$uid'")->delete();
            ReAjax(1,"取消收藏");
        }else{
            $data['qid']=$qid;
            $data['uid']=$uid;
            $data['create_time']=time(); 
            M('collect')->add($data);
            ReAjax(0,'收藏成功');
        }
    }

    public function zan()
    {
        $aid = I('post.aid');

        $uid = $_SESSION['uid'];

        $res = M('zan')->where("aid='$aid' and uid='$uid'")->find();

        if($res){
            M('zan')->where("aid='$aid' and uid='$uid'")->delete();
            ReAjax(1,"取消点贊");
        }else{
            $data['aid']=$aid;
            $data['uid']=$uid;
            $data['create_time']=time(); 
            M('zan')->add($data);
            ReAjax(0,'点贊成功');
        }
    }

    public function addanswer()
    {
        $data = I('post.');

        $data['uid']=$_SESSION['uid'];
        $data['create_time']=time(); 

        if(M('answer')->add($data)){
            ReAjax(0,'回复成功');
        }else{
            ReAjax(1,'回复成功');
        }
    }
}