<?php
namespace Index\Controller;
use Think\Controller;
class UController extends CommonController {
    public function index(){
    	$user = M('user')->find(I('get.id'));
    	$this->assign('user',$user);
    	$this->assign('title',$user['nickname']."个人中心");
        $this->display();
    }
}