create table user(
	id int(5) PRIMARY KEY AUTO_INCREMENT,
	username char(20),
	email char(30),
	password char(32),
	create_time int(10)
);

create table msg(
	id int(5) PRIMARY KEY AUTO_INCREMENT,
	uid int(5),
	content text,
	create_time int(10)
);



标签表

CREATE TABLE `tag` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL COMMENT '标签名',
  `url` char(100) NOT NULL COMMENT '路径名',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

标签与文章中间表

CREATE TABLE `atr_tag` (
  `aid` int(10) NOT NULL COMMENT '文章id',
  `tid` int(10) NOT NULL COMMENT '标签id',
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

文章表  

CREATE TABLE `atr` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `uid` int(5) NOT NULL COMMENT '用户ID',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

用户表

CREATE TABLE `user` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `username` char(20) DEFAULT NOT NULL COMMENT '用户名',
  `city` char(20) DEFAULT NOT NULL COMMENT '城市',
  `blog_url` char(80) DEFAULT  NOT NULL COMMENT '博客地址',
  `face` char(80) DEFAULT  NOT NULL COMMENT '头像',
  `atr_num` int(5) NOT NULL COMMENT '文章数',
  `create_time` int(10) NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

友情链接

CREATE TABLE `link` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(20) DEFAULT NOT NULL COMMENT '链接名',
  `url` char(80) DEFAULT  NOT NULL COMMENT '地址',
  `is_target` int(1) DEFAULT 0 COMMENT '是否新窗口打开',
  `sort` int(2) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

导航表

CREATE TABLE `nav` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(20) DEFAULT NOT NULL COMMENT '链接名',
  `url` char(80) DEFAULT  NOT NULL COMMENT '地址',
  `is_target` int(1) DEFAULT 0 COMMENT '是否新窗口打开',
  `sort` int(2) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



-- 用户表

CREATE TABLE `user` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nickname` char(30)  NOT NULL COMMENT '昵称',
  `email` char(50)  NOT NULL COMMENT '邮箱',
  `password` char(32)  NOT NULL COMMENT '密码',
  `face` char(80)   NOT NULL COMMENT '头像',
  `sex` enum("男",'女') DEFAULT "男" NOT NULL COMMENT '性别',
  `openid` char(32)  NOT NULL COMMENT '第三方id',
  `city` char(20)  NOT NULL COMMENT '城市',
  `kiss` int(5) NOT NULL COMMENT '飞吻',
  `sign` char(100)  NOT NULL COMMENT '签名',
  `question_num` int(5) NOT NULL COMMENT '提问数',
  `answer_num` int(5) NOT NULL COMMENT '提问数',
  `level` int(1) DEFAULT 0 NOT NULL COMMENT '等级(0,普通会员，1:vip1)',
  `create_time` int(10) NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "用户表";


-- 提问表
CREATE TABLE `question` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `title` char(30)  NOT NULL COMMENT '标题',
  `content` text  NOT NULL COMMENT '内容',
  `uid` int(5)  NOT NULL COMMENT '用户id',
  `cid` int(5)  NOT NULL COMMENT '分类id',
  `status` enum("精华",'置顶','未结贴','已结贴') DEFAULT "未结贴" NOT NULL COMMENT '状态',
  `kiss` int(5) DEFAULT 0 NOT NULL COMMENT '飞吻',
  `view_num` int(5) DEFAULT 0 NOT NULL COMMENT '浏览量',
  `answer_num` int(5) DEFAULT 0 NOT NULL COMMENT '回答量',
  `create_time` int(10) NOT NULL COMMENT '提问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "提问表";


-- 回答表

CREATE TABLE `answer` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `content` text  NOT NULL COMMENT '内容',
  `uid` int(5)  NOT NULL COMMENT '用户id',
  `qid` int(5)  NOT NULL COMMENT '问题id',
  `status` enum('0','1') DEFAULT "0" NOT NULL COMMENT '状态:0未采纳，1采纳',
  `zan_num` int(5) DEFAULT 0 NOT NULL COMMENT '点赞量',
  `create_time` int(10) NOT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "回答表";

-- 分类表
CREATE TABLE `category` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(20)  NOT NULL COMMENT '分类名',
  `fid` int(5) DEFAULT  0 NOT NULL COMMENT '父id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "分类表";

-- 友情链接表

CREATE TABLE `link` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(20)  NOT NULL COMMENT '链接名',
  `url` char(80)   NOT NULL COMMENT '地址',
  `is_target` int(1) DEFAULT 0 COMMENT '是否新窗口打开',
  `sort` int(2) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "友情链接表";

-- 案例表


CREATE TABLE `anli` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `title` char(20)  NOT NULL COMMENT '标题',
  `info` char(80)   NOT NULL COMMENT '描述',
  `uid` int(5)  NOT NULL COMMENT '用户id',
  `img` char(80) NOT NULL COMMENT '缩略图',
  `create_time` int(10) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "案例表";

-- 收藏表
CREATE TABLE `collect` (
  `uid` int(10) NOT NULL COMMENT '用户id',
  `qid` int(10) NOT NULL COMMENT '提问id',
  `create_time` int(10) NOT NULL COMMENT '收藏时间',
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "收藏表";


-- 验证表

CREATE TABLE `vercode` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `question` char(30) NOT NULL COMMENT '问题',
  `answer` char(10) NOT NULL COMMENT '答案',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "验证表";

-- 点赞表

CREATE TABLE `zan` (
  `uid` int(10) NOT NULL COMMENT '用户id',
  `aid` int(10) NOT NULL COMMENT '回答id',
  `create_time` int(10) NOT NULL COMMENT '收藏时间',
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "点赞表";