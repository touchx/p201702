<?php
namespace app\home\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\console\input\Argument;
use think\Db;

class Copy extends Command
{
    protected function configure()
    {
    	$this->addArgument('start', Argument::OPTIONAL);//可选参数
    	$this->addArgument('end', Argument::OPTIONAL);//可选参数

        $this->setName('copy')->setDescription('this is copy');
    }

    protected function execute(Input $input, Output $output)
    {

    	$start = $input->getArguments()['start'];
    	$end = $input->getArguments()['end'];

        for ($i=$start; $i <$end ; $i++) { 

        	vendor("phpquery.phpQuery");

			\phpQuery::newDocumentFile('http://fly.layui.com/jie/'.$i.'/'); 

			$title =  pq(".detail h1")->html(); 

			if($title){
				Db::execute('insert into question ( title,content) values (?, ?)',[$title,'thinkphp@qq.com']);
			}

			if($i%100==0){
        		echo $i.PHP_EOL;
        	}

        	
        }

    }
}